package general;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

import com.google.gson.Gson;

public class crearJSON {

	public static LinkedList<Atleta> atletas = new LinkedList<Atleta>();
	public static Gson gson = new Gson();
	
	public static void main(String[] args) {
		atletas.add(new Atleta("Andr�s �lvarez","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Bruno Berger","Masculino","Chileno","Volley"));
		atletas.add(new Atleta("Carlos Costa","Masculino","Chileno","Volley"));
		atletas.add(new Atleta("David Donda","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Erik Erikson","Masculino","Alem�n","Futbol"));
		atletas.add(new Atleta("Francisco Fitz","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Andrea Alvarez","Femenino","Argentina","Futbol"));
		atletas.add(new Atleta("Breatriz Bueno","Femenino","Argentina","Futbol"));		
		atletas.add(new Atleta("Carolina Cortez","Femenino","Argentina","Futbol"));		
		atletas.add(new Atleta("Dilma Dusao","Femenino","Brasilera","Volley"));		
		atletas.add(new Atleta("Emma Eubino","Femenino","Brasilera","Volley"));		
		atletas.add(new Atleta("Fernanda Fernandez","Femenino","Chilena","Futbol"));		
		atletas.add(new Atleta("Gisela Gonzalez","Femenino","Argentina","Futbol"));		
		atletas.add(new Atleta("Guillermo Gonz�lez","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Hugo Hack","Masculino","Chileno","Futbol"));
		atletas.add(new Atleta("Itsuki Inazagi","Masculino","Japones","Natacion"));
		atletas.add(new Atleta("Jon�s Jonhson","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Katsuki Kirishima","Masculino","Japones","Natacion"));
		atletas.add(new Atleta("Luis Liba","Masculino","Argentino","Volley"));
		atletas.add(new Atleta("Mat�as Mart�nez","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Hada Huemes","Femenino","Chilena","Hotckey"));		
		atletas.add(new Atleta("India Iris","Femenino","Chilena","Natacion"));	
		atletas.add(new Atleta("Ramiro Rodr�guez","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Julieta Jons","Femenino","Argentina","Volley"));
		atletas.add(new Atleta("Karen Karh","Femenino","Argentina","Volley"));		
		atletas.add(new Atleta("Nicol�s Nichel","Masculino","Chileno","Volley"));
		atletas.add(new Atleta("Orlando Ore","Masculino","Chileno","Volley"));
		atletas.add(new Atleta("Luisa Lopez","Femenino","Argentina","Futbol"));
		atletas.add(new Atleta("Mariana Martinez","Femenino","Argentina","Futbol"));		
		atletas.add(new Atleta("Natalie Night","Femenino","Argentina","Futbol"));		
		atletas.add(new Atleta("Sabrina ","Femenino","Argentina","Hotckey"));		
		atletas.add(new Atleta("Oriana Orwell","Femenino","Brasilera","Volley"));		
		atletas.add(new Atleta("Paola Pull","Femenino","Brasilera","Volley"));		
		atletas.add(new Atleta("Quina Queue","Femenino","Chilena","Futbol"));		
		atletas.add(new Atleta("Rocio","Femenino","Argentina","Futbol"));		
		atletas.add(new Atleta("Pablo Pugliese","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Queso Queue","Masculino","Argentino","Futbol"));		
		atletas.add(new Atleta("Walter Walls","Masculino","Argentino","Volley"));
		atletas.add(new Atleta("Xion Xilo","Masculino","Argentino","Volley"));
		atletas.add(new Atleta("Haru Yoshida","Masculino","Japones","Natacion"));
		atletas.add(new Atleta("Zacarias Zapata","Masculino","Chileno","Volley"));
		atletas.add(new Atleta("Santiago Sanchez","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Teo Tallison","Masculino","Chileno","Futbol"));
		atletas.add(new Atleta("Uma Usagi","Masculino","Japones","Natacion"));
		atletas.add(new Atleta("Victor Vork","Masculino","Argentino","Futbol"));		
		atletas.add(new Atleta("Tatiana Toledo","Femenino","Argentina","Volley"));
		atletas.add(new Atleta("Uria Ulo","Femenino","Argentina","Volley"));
		atletas.add(new Atleta("Viviana Vera","Femenino","Argentina","Futbol"));
		atletas.add(new Atleta("Zaira Zoraa","Femenino","Argentina","Futbol"));	
		
		String JSON2 = gson.toJson(atletas);	
		
		try{
			File file = new File("listado1");
			if (file.exists()){
				throw new IllegalArgumentException("Ya existe un listado con ese nombre.");
			}else{
				file.createNewFile();
			}			
			try{
				FileOutputStream fos =  new FileOutputStream("listado1");
				ObjectOutputStream out =  new ObjectOutputStream(fos);
				out.writeObject(JSON2);
				out.close();		
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

}
