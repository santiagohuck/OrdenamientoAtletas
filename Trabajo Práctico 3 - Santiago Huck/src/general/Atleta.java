package general;

public class Atleta  implements Comparable<Atleta>{
	
	private String nombre;
	private String genero;
	private String nacionalidad;
	private String deporte;
	
	public static Atleta vacio(){
		return new Atleta("","","","");
	}
	
	public Atleta(String nombre, String genero, String nacionalidad, String deporte){
		
		setNombre(nombre);
		setGenero(genero);
		setNacionalidad(nacionalidad);
		setDeporte(deporte);
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getDeporte() {
		return deporte;
	}

	public void setDeporte(String deporte) {
		this.deporte = deporte;
	}
	
	@Override
	public int compareTo(Atleta otro) {
		if (getGenero().compareTo(otro.getGenero())==0){
			if (getNacionalidad().compareTo(otro.getNacionalidad())==0){
				return getDeporte().compareTo(otro.getDeporte());
			}else{
				return getNacionalidad().compareTo(otro.getNacionalidad());
			}			
		}else{
			return getGenero().compareTo(otro.getGenero());
		}
	}	
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("Nombre: "+getNombre()+" - ");
		sb.append("G�nero: "+getGenero()+" - ");
		sb.append("Nacionalidad: "+getNacionalidad()+" - ");
		sb.append("Deporte: "+getDeporte());		
		return sb.toString();
	}

	

}
