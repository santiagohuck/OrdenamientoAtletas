package general;

import java.util.LinkedList;

public class Departamento {
	
	private int numero;
	private Atleta[] camas;	
	private int fitness;
	
	public Departamento(int num){
		
		setNumero(num);
		camas = new Atleta[4];		
		fitness = 0;
		
	}
	
    public Departamento(LinkedList<Atleta> lista, int num){
		
		setNumero(num);
    	camas = new Atleta[4];
    	if (lista.size()>4){
    		throw new IllegalArgumentException("Un departamento tiene 4 camas y la lista ingresada tiene "+lista.size()+" atletas!!!");
    	}
		for(int i=0; i<lista.size();i++){
			asignarCama(lista.get(i));
		}		
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}	
	
	public int getFitness() {
		return fitness;
	}

	public void setFitness(int fitness) {
		this.fitness = fitness;
	}

	public Atleta getCama(int i) {
		return camas[i];
	}

	public void setCama(int i, Atleta a) {
		this.camas[i] = a;
	}
	public boolean asignarCama(Atleta a){
		if (primerDisponible()>=0){
			setCama(primerDisponible(),a);
			return true;
		}
		return false;		
	}
	
	public int camasDisponibles(){
		int cant =0;
		for (int i=0; i<4; i++){
			if (getCama(i)==null){
				cant+=1;
			}
		}	
		return cant;
	}	
	
	private int primerDisponible(){
		if (camasDisponibles()==0){
			return -1;
		}else{
			for (int i=0; i<4; i++){
				if (getCama(i)==null){
					return i;
				}
			}	
		}
		return -1;
	}
	
	public void autocompletarse(){
		for (int i=0; i<4; i++){
			if (getCama(i)==null){
				setCama(i,Atleta.vacio());
			}
		}
	}
	
	private boolean mismoPais(){
		return getCama(0).getNacionalidad().equals(getCama(1).getNacionalidad()) && 
				getCama(0).getNacionalidad().equals(getCama(2).getNacionalidad()) 
				&& getCama(0).getNacionalidad().equals(getCama(3).getNacionalidad()); 
	}
	
	private boolean mismoDeporte(){
		return getCama(0).getDeporte().equals(getCama(1).getDeporte()) && 
				getCama(0).getDeporte().equals(getCama(2).getDeporte()) && 
				getCama(0).getDeporte().equals(getCama(3).getDeporte()); 
	}
	
	public void obtenerFitness(){
		if (mismoPais()){
			fitness+=5;
		}
		if (mismoDeporte()){
			fitness+=5;
		}
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("Departamento "+numero+": \n");
		for (int i=1; i<5; i++){
			sb.append("[Cama "+i+": "+getCama(i-1).toString()+"]\n");			
		
		}
		sb.append("Puntaje de asignación: "+fitness+".\n");
		return sb.toString();
	}
}
