package general;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

public class DepartamentoTest {
	
	LinkedList<Atleta> atletas = new LinkedList<Atleta>();

	@Test
	public void deptoVacioTest() {
		Departamento uno = new Departamento(0);
		assertTrue(uno.camasDisponibles()==4);
	}
	
	@Test
	public void deptoLlenoTest() {
		for (int i=0;i<4;i++){
			atletas.add(Atleta.vacio());
		}		
		Departamento uno = new Departamento(atletas,0);
		assertTrue(uno.camasDisponibles()==0);
	}
	
	@Test
	public void numeroTest() {
		Departamento uno = new Departamento(5);
		assertTrue(uno.getNumero()==5);
	}
	
	@Test
	public void fitnessTest() {
		for (int i=0;i<4;i++){
			atletas.add(Atleta.vacio());
		}		
		Departamento uno = new Departamento(atletas,0);
		uno.obtenerFitness();
		assertTrue(uno.getFitness()==10);
	}
	
	@Test
	public void autocompletarseTest() {
		for (int i=0;i<2;i++){
			atletas.add(Atleta.vacio());
		}		
		Departamento uno = new Departamento(atletas,0);
		uno.autocompletarse();
		assertTrue(uno.camasDisponibles()==0);
	}

}
