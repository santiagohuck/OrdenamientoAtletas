package ordenamientoPrevio;

import general.Atleta;
import general.Departamento;

import java.util.Collections;
import java.util.LinkedList;

public class DistribuidorOP {
	
	private LinkedList<Atleta> atletas;
	private LinkedList<Departamento> departamentos;
	private int totalDeptos = 0;
	
	public DistribuidorOP(LinkedList<Atleta> atletas){
		
		if (atletas.isEmpty()){
			throw new IllegalArgumentException("El listado est� vacio!!!");
		}else{
			setAtletas(atletas);
			Collections.sort(atletas);
			departamentos = new LinkedList<Departamento>();
		}		
		
	}

	public LinkedList<Atleta> getAtletas() {
		return atletas;
	}

	public void setAtletas(LinkedList<Atleta> atletas) {
		this.atletas = atletas;
	}
	
	public LinkedList<Departamento> getDepartamentos() {
		return departamentos;
	}

	public void setDepartamentos(LinkedList<Departamento> departamentos) {
		this.departamentos = departamentos;
	}

	public void distribuir(){
		distribuirDeptosOptimos();
		distribuirDeptosAceptables();
		distribuirDeptosRegulares();
		obtenerFitness();
	}
	
	private void distribuirDeptosOptimos(){
		LinkedList<Atleta> cola = new LinkedList<Atleta>();
		LinkedList<Atleta> eliminables = new LinkedList<Atleta>();
		for (int i=0; i<atletas.size();i++){
			if (cola.isEmpty()){
				cola.add(atletas.get(i));
			}else if (atletas.get(i).compareTo(cola.getFirst())==0){
				if (cola.size()<3){		
					cola.add(atletas.get(i));
				}else{
					cola.add(atletas.get(i));
					totalDeptos +=1;
					departamentos.add(new Departamento(cola,totalDeptos));
					eliminables.addAll(cola);
					cola.clear();						
				}
			}else{
				if (cola.size()==4){
					totalDeptos +=1;
					departamentos.add(new Departamento(cola,totalDeptos));
					eliminables.addAll(cola);
					cola.clear();	
					cola.add(atletas.get(i));
				}else{
					cola.clear();
					cola.add(atletas.get(i));
				}				
			}
		}
		atletas.removeAll(eliminables);
	}
	
	void distribuirDeptosAceptables(){
		LinkedList<Atleta> cola = new LinkedList<Atleta>();
		LinkedList<Atleta> eliminables = new LinkedList<Atleta>();
		for (int i=0; i<atletas.size();i++){
			if (cola.isEmpty()){
				cola.add(atletas.get(i));
			}else if (atletas.get(i).getGenero().equals(cola.getFirst().getGenero()) && atletas.get(i).getNacionalidad().equals(cola.getFirst().getNacionalidad())){
					if (cola.size()<3){		
						cola.add(atletas.get(i));
					}else{
						cola.add(atletas.get(i));
						totalDeptos +=1;
						departamentos.add(new Departamento(cola,totalDeptos));
						eliminables.addAll(cola);	
						cola.clear();
						
					}
			}else{
				if (cola.size()==4){
					totalDeptos +=1;
					departamentos.add(new Departamento(cola,totalDeptos));
					eliminables.addAll(cola);	
					cola.clear();
					cola.add(atletas.get(i));					
				}else{
					cola.clear();
					cola.add(atletas.get(i));
					
				}				
			}
		}
		atletas.removeAll(eliminables);
	}
	
	void distribuirDeptosRegulares(){
		LinkedList<Atleta> cola = new LinkedList<Atleta>();
		LinkedList<Atleta> eliminables = new LinkedList<Atleta>();
		for (int i=1; i<atletas.size();i++){
			if (cola.isEmpty()){
				cola.add(atletas.get(i));
			}else if (atletas.get(i).getGenero().equals(cola.getFirst().getGenero())){
					if (cola.size()<3){		
						cola.add(atletas.get(i));
						if (i==atletas.size()-1){
							totalDeptos +=1;
							departamentos.add(new Departamento(cola,totalDeptos));
							departamentos.getLast().autocompletarse();
							eliminables.addAll(cola);	
						}
					}else{
						cola.add(atletas.get(i));
						totalDeptos +=1;
						departamentos.add(new Departamento(cola,totalDeptos));
						eliminables.addAll(cola);	
						cola.clear();						
					}
			}else{
				if (cola.size()==4){									
					totalDeptos +=1;
					departamentos.add(new Departamento(cola,totalDeptos));
					cola.clear();
					cola.add(atletas.get(i));	
				}else{
					totalDeptos +=1;
					departamentos.add(new Departamento(cola,totalDeptos));
					departamentos.getLast().autocompletarse();
					cola.clear();
					cola.add(atletas.get(i));						
					eliminables.addAll(cola);							
				}				
				
			}
		}
		atletas.removeAll(eliminables);
	}
	
	void obtenerFitness(){
		for (int i=0;i<departamentos.size();i++){
			departamentos.get(i).obtenerFitness();
		}
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<departamentos.size();i++){
			sb.append(departamentos.get(i).toString()+"\n");
		}		
		return sb.toString();
	}
	

}
