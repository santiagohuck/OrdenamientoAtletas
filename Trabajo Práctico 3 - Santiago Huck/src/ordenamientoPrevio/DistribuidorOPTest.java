package ordenamientoPrevio;

import static org.junit.Assert.*;
import general.Atleta;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class DistribuidorOPTest {

	LinkedList<Atleta> atletas = new LinkedList<Atleta>();
	
	@Before
	public void generar(){
		atletas.add(new Atleta("Andr�s �lvarez","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Bruno Berger","Masculino","Chileno","Volley"));
		atletas.add(new Atleta("Carlos Costa","Masculino","Chileno","Volley"));
		atletas.add(new Atleta("David Donda","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Francisco Fitz","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Andrea Alvarez","Femenino","Argentina","Futbol"));
		atletas.add(new Atleta("Breatriz Bueno","Femenino","Argentina","Futbol"));		
		atletas.add(new Atleta("Carolina Cortez","Femenino","Argentina","Futbol"));		
		atletas.add(new Atleta("Dilma Dusao","Femenino","Brasilera","Volley"));		
		atletas.add(new Atleta("Emma Eubino","Femenino","Brasilera","Volley"));				
		atletas.add(new Atleta("Gisela Gonzalez","Femenino","Argentina","Futbol"));		
		atletas.add(new Atleta("Guillermo Gonz�lez","Masculino","Argentino","Futbol"));
		atletas.add(new Atleta("Hugo Hack","Masculino","Chileno","Futbol"));		
		atletas.add(new Atleta("Nicol�s Nichel","Masculino","Chileno","Volley"));
		
	}	
	
	@Test(expected = IllegalArgumentException.class)
	public void listadoVacioTest(){
		LinkedList<Atleta> vacio = new LinkedList<Atleta>();
		@SuppressWarnings("unused")
		DistribuidorOP uno = new DistribuidorOP(vacio);			
	}
	
	@Test
	public void distribuirTest(){		
		DistribuidorOP uno = new DistribuidorOP(atletas);	
		uno.distribuir();
		assertTrue(uno.getDepartamentos().size()==13);
		
	}
	
	@Test
	public void fitnessEsperadoTest(){
		DistribuidorOP uno = new DistribuidorOP(atletas);
		int cont =0;
		uno.distribuir();	
		for (int i=0; i<uno.getDepartamentos().size();i++){
			cont+=uno.getDepartamentos().get(i).getFitness();
		}
		assertTrue(cont==25);
	}
	

}
