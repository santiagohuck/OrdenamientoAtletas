package primerOptimo;

import general.Atleta;
import general.Departamento;

import java.util.LinkedList;

public class DistribuidorPO {
	
	private LinkedList<Atleta> atletas;
	private LinkedList<Departamento> departamentos;
	private int totalDeptos = 1;
	
	public DistribuidorPO(LinkedList<Atleta> atletas){
		
		if (atletas.isEmpty()){
			throw new IllegalArgumentException("El listado est� vacio!!!");
		}else{
			setAtletas(atletas);
			departamentos = new LinkedList<Departamento>();
		}
				
	}

	public LinkedList<Atleta> getAtletas() {
		return atletas;
	}

	public void setAtletas(LinkedList<Atleta> atletas) {
		this.atletas = atletas;
	}
	
	public LinkedList<Departamento> getDepartamentos() {
		return departamentos;
	}

	public void setDepartamentos(LinkedList<Departamento> departamentos) {
		this.departamentos = departamentos;
	}

	public void distribuir(){
		departamentos.add(new Departamento(totalDeptos));
		while (atletas.isEmpty()==false){
			if (departamentos.getLast().camasDisponibles()==4){
				departamentos.getLast().asignarCama(atletas.getFirst());
				atletas.pollFirst();
			}else if (departamentos.getLast().camasDisponibles()==0){
				totalDeptos++;
				departamentos.add(new Departamento(totalDeptos));
				departamentos.getLast().asignarCama(atletas.getFirst());
				atletas.pollFirst();
			}else{
				Atleta aux = mejorPosible(departamentos.getLast().getCama(0));
				departamentos.getLast().asignarCama(aux);
				atletas.remove(aux);
			}
		}
		departamentos.getLast().autocompletarse();
		obtenerFitness();		
	}
	
	public Atleta mejorPosible(Atleta atleta){
		for (int i=0; i<atletas.size();i++){
			if (atleta.compareTo(atletas.get(i))==0){
				return atletas.get(i);
			}
		}
		for (int i=0; i<atletas.size();i++){
			if (atleta.getGenero().compareTo(atletas.get(i).getGenero())==0	&& 
				atleta.getNacionalidad().compareTo(atletas.get(i).getNacionalidad())==0){
				return atletas.get(i);
			}
		}
		for (int i=0; i<atletas.size();i++){
			if (atleta.getGenero().compareTo(atletas.get(i).getGenero())==0){
				return atletas.get(i);
			}
		}
		return Atleta.vacio();
	}	
	
	
	private void obtenerFitness(){
		for (int i=0;i<departamentos.size();i++){
			departamentos.get(i).obtenerFitness();
		}
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<departamentos.size();i++){
			sb.append(departamentos.get(i).toString()+"\n");
		}		
		return sb.toString();
	}
	

}

