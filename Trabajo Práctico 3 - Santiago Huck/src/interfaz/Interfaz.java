package interfaz;

import general.Atleta;
import general.Departamento;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.util.LinkedList;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import ordenamientoPrevio.DistribuidorOP;
import primerOptimo.DistribuidorPO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class Interfaz {

	private JFrame ventana;
	private JPanel panel;
	private JTable tablaAtletas, tablaDepartamentos;
	private JScrollPane scrollPane1, scrollPane2;
	private JButton btnDistribucionOP, btnCargarListado, btnDistribucionPO, btnSalir;
	private JLabel lblDistribuidor, lblPO, lblOP;
	private DistribuidorOP distribuidorOP;
	private DistribuidorPO distribuidorPO;
	private LinkedList<Atleta> atletas;
	private String JSON;
	private Gson gson;	
	private boolean distribuidoOP, distribuidoPO;
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.ventana.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Interfaz() {
		iniciar();
		
	}
	
	private void iniciar() {
		iniciarVentana();
		iniciarDistribuidores();		
		iniciarLabels();		
		iniciarBotones();		
		iniciarScrollPanel();				
		iniciarTablas();
	}
	
	private void iniciarDistribuidores() {
		atletas = new LinkedList<Atleta>();
		gson = new GsonBuilder().setPrettyPrinting().create();
		distribuidoOP = false;
		distribuidoPO = false;
		JSON="";
	}
	
	private void iniciarVentana() {
		ventana = new JFrame();
		ventana.setBounds(10, 100, 1350, 550);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.getContentPane().setLayout(null);
		panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(896, 11, 428, 206);
		ventana.getContentPane().add(panel);
	}

	private void iniciarLabels() {
		lblDistribuidor = new JLabel("Distribuidor");
		lblDistribuidor.setHorizontalAlignment(SwingConstants.CENTER);
		lblDistribuidor.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblDistribuidor.setBounds(131, 1, 172, 23);
		panel.add(lblDistribuidor);
		
		lblPO = new JLabel("PO = Primer \u00D3ptimo   ");
		lblPO.setHorizontalAlignment(SwingConstants.CENTER);
		lblPO.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPO.setBounds(23, 139, 194, 23);
		panel.add(lblPO);
		
		lblOP = new JLabel("OP = Ordenamiento Previo");
		lblOP.setHorizontalAlignment(SwingConstants.CENTER);
		lblOP.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblOP.setBounds(10, 167, 207, 23);
		panel.add(lblOP);
	}

	private void iniciarBotones() {
		btnDistribucionOP = new JButton("Generar Distribuci\u00F3n OP");
		btnDistribucionOP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (distribuidoOP == false){
					Type listType = new TypeToken<LinkedList<Atleta>>(){}.getType();
		 			atletas = gson.fromJson(JSON, listType);
					distribuidorOP = new DistribuidorOP(atletas);
					distribuidorOP.distribuir();
					distribuidoOP = true;
					String n = gson.toJson(distribuidorOP.getDepartamentos());
					tablaDepartamentos.setModel(modeloTablaDepartamentos(distribuidorOP.getDepartamentos()));
					tablaDepartamentos.getColumnModel().getColumn(0).setMaxWidth(40);
					tablaDepartamentos.getColumnModel().getColumn(5).setMaxWidth(75);
					DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
					centerRenderer.setHorizontalAlignment( JLabel.CENTER );
					tablaDepartamentos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
					tablaDepartamentos.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
					try{
						String nombre = JOptionPane.showInputDialog("Nombre de la distribución: ");
						File file = new File(nombre);
						if (file.exists()){
							JOptionPane.showMessageDialog(ventana, "Ya existe una distribución con ese nombre.");
							distribuidoOP = false;
							throw new IllegalArgumentException("Ya existe una distribución con ese nombre.");
						}else{
							file.createNewFile();
						}			
						try{
							FileOutputStream fos =  new FileOutputStream(nombre);
							ObjectOutputStream out =  new ObjectOutputStream(fos);
							out.writeObject(n);
							out.close();		
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else{
					JOptionPane.showMessageDialog(ventana, "La distribución ya ha sido realizada.");
					throw new IllegalArgumentException("La distribución ya ha sido realizada.");
				}
			}
		});
		btnDistribucionOP.setBounds(230, 80, 188, 23);
		panel.add(btnDistribucionOP);
		
		btnDistribucionPO = new JButton("Generar Distribuci\u00F3n PO");
		btnDistribucionPO.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		if (distribuidoPO == false){
		 			Type listType = new TypeToken<LinkedList<Atleta>>(){}.getType();
		 			atletas = gson.fromJson(JSON, listType);
					distribuidorPO = new DistribuidorPO(atletas);
					distribuidorPO.distribuir();
					distribuidoPO = true;
					String n = gson.toJson(distribuidorPO.getDepartamentos());
					tablaDepartamentos.setModel(modeloTablaDepartamentos(distribuidorPO.getDepartamentos()));
					tablaDepartamentos.getColumnModel().getColumn(0).setMaxWidth(40);
					tablaDepartamentos.getColumnModel().getColumn(5).setMaxWidth(75);
					DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
					centerRenderer.setHorizontalAlignment( JLabel.CENTER );
					tablaDepartamentos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
					tablaDepartamentos.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
					try{
						String nombre = JOptionPane.showInputDialog("Nombre de la distribución: ");
						File file = new File(nombre);
						if (file.exists()){
							JOptionPane.showMessageDialog(ventana, "Ya existe una distribución con ese nombre.");
							distribuidoPO = false;
							throw new IllegalArgumentException("Ya existe una distribución con ese nombre.");
						}else{
							file.createNewFile();
						}			
						try{
							FileOutputStream fos =  new FileOutputStream(nombre);
							ObjectOutputStream out =  new ObjectOutputStream(fos);
							out.writeObject(n);
							out.close();		
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else{
					JOptionPane.showMessageDialog(ventana, "La distribución ya ha sido realizada.");
					throw new IllegalArgumentException("La distribución ya ha sido realizada.");
				}
		 	}
		 });
		btnDistribucionPO.setBounds(230, 35, 188, 23);
		panel.add(btnDistribucionPO);
		
		btnCargarListado = new JButton("Cargar Listado");
		btnCargarListado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nombre = JOptionPane.showInputDialog("Nombre del listado: ");
				JSON = "";
				Type listType = new TypeToken<LinkedList<Atleta>>(){}.getType();
				try{
					File file = new File(nombre);		
					if (!file.exists()){
						JOptionPane.showMessageDialog(ventana, "No existe un listado guardado con ese nombre.");
						throw new IllegalArgumentException("No existe un listado guardado con ese nombre.");
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				try{
					FileInputStream fos =  new FileInputStream(nombre);
					ObjectInputStream in =  new ObjectInputStream(fos);					
					JSON = ((String) in.readObject());
					in.close();
				}catch(Exception ex){
					ex.printStackTrace();
				}
				atletas.clear();
				atletas = gson.fromJson(JSON, listType);
				tablaAtletas.setModel(modeloTablaAtletas(atletas));
				distribuidoOP = false;
				distribuidoPO = false;
				tablaDepartamentos.setModel(modeloTablaVacio());
				
			}
		});
		btnCargarListado.setBounds(23, 35, 172, 23);
		panel.add(btnCargarListado);
		
		btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnSalir.setBounds(252, 152, 141, 23);
		panel.add(btnSalir);
		
	}

	private void iniciarScrollPanel() {
		scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(10, 11, 876, 206);
		ventana.getContentPane().add(scrollPane1);
		scrollPane2 = new JScrollPane();
		scrollPane2.setBounds(10, 228, 1314, 273);
		ventana.getContentPane().add(scrollPane2);
	}

	private void iniciarTablas() {
		tablaAtletas = new JTable();
		scrollPane1.setViewportView(tablaAtletas);				
		tablaDepartamentos = new JTable();
		scrollPane2.setViewportView(tablaDepartamentos);
	}
	
	public LinkedList<Atleta> getAtletas(){
		return atletas;
	}
	
	public DefaultTableModel modeloTablaAtletas(LinkedList<Atleta> linkedList)
	{
		
		DefaultTableModel tableModel = new DefaultTableModel();
		tableModel.addColumn("Nombre");
		tableModel.addColumn("Género");
		tableModel.addColumn("Nacionalidad");
		tableModel.addColumn("Deporte");
		
			    
		for (int i=0; i<linkedList.size();i++){
			tableModel.addRow(new String[] {linkedList.get(i).getNombre(),linkedList.get(i).getGenero(),
										linkedList.get(i).getNacionalidad(),linkedList.get(i).getDeporte()});
		}
		return tableModel;	
		
	}
	
	public DefaultTableModel modeloTablaDepartamentos(LinkedList<Departamento> departamentos)
	{
		
		DefaultTableModel tableModel = new DefaultTableModel();
		tableModel.addColumn("N°");
		tableModel.addColumn("Cama 1");
		tableModel.addColumn("Cama 2");
		tableModel.addColumn("Cama 3");
		tableModel.addColumn("Cama 4");	
		tableModel.addColumn("Fitness");	
			    
		for (int i=0; i<departamentos.size();i++){
			tableModel.addRow(new String[] {departamentos.get(i).getNumero()+"",departamentos.get(i).getCama(0).getNombre()+" - "+
					departamentos.get(i).getCama(0).getGenero()+" - "+departamentos.get(i).getCama(0).getNacionalidad()+" - "+
					departamentos.get(i).getCama(0).getDeporte(),departamentos.get(i).getCama(1).getNombre()+" - "+
							departamentos.get(i).getCama(1).getGenero()+" - "+departamentos.get(i).getCama(1).getNacionalidad()+" - "+
							departamentos.get(i).getCama(1).getDeporte(),departamentos.get(i).getCama(2).getNombre()+" - "+
									departamentos.get(i).getCama(2).getGenero()+" - "+departamentos.get(i).getCama(2).getNacionalidad()+" - "+
									departamentos.get(i).getCama(2).getDeporte(),departamentos.get(i).getCama(3).getNombre()+" - "+
											departamentos.get(i).getCama(3).getGenero()+" - "+departamentos.get(i).getCama(3).getNacionalidad()+" - "+
											departamentos.get(i).getCama(3).getDeporte(),departamentos.get(i).getFitness()+""});
		}
		int cont=0;
		for (int i=0; i<departamentos.size();i++){
			cont+=departamentos.get(i).getFitness();
		}
		tableModel.addRow(new String[] {"Total","","","","",cont+""});
		return tableModel;	
		
	}
	
	public DefaultTableModel modeloTablaVacio()
	{
		
		DefaultTableModel tableModel = new DefaultTableModel();
		tableModel.addColumn("N°");
		tableModel.addColumn("Cama 1");
		tableModel.addColumn("Cama 2");
		tableModel.addColumn("Cama 3");
		tableModel.addColumn("Cama 4");	
		tableModel.addColumn("Fitness");	
			    
		tableModel.addRow(new String[] {"","","","","",""});
		tableModel.addRow(new String[] {"Total","","","","",""});
		return tableModel;	
		
	}
}
